Rails.application.routes.draw do
  devise_for :employees
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  root to: "admin/dashboard#index"
  
  namespace :api do

    ### employee
    devise_for :employees
    devise_scope :employee do
      post 'employees/sign_up' , to: 'registrations#create'
      post 'employees/sign_in' , to: 'sessions#create'
      delete 'employees/logout' , to: 'sessions#destroy'
      post 'employees/forgot_password' , to: 'sessions#forgot_password'
    end
    resources :employees, :only => [:index, :show, :update]
    get 'employees/:id/history' , to: 'employees#history'
    put 'employees/:id/change_password', to: 'employees#change_password'
    post 'employees/:id/add_phones', to: 'employees#add_phones'
    post 'employees/:id/attendances', to: 'employees#attendances'
    post 'employees/:id/web_attendances', to: 'employees#web_attendances'
    post 'employees/:id/overtimes', to: 'employees#overtimes'
    post 'employees/:id/leaves', to: 'employees#leaves'
    get '/report_tos', to: 'employees#report_tos'
    get '/approved_bys', to: 'employees#approved_bys'
    resources :payrolls, :only => [:index] 
    post 'payrolls/payroll', to: 'payrolls#payroll'
    post 'payrolls/detail_payroll', to: 'payrolls#detail_payroll'
    resources :departments, :only => [:index] 
    resources :titles, :only => [:index]

    ### atendance
    resources :attendances, :only => [:index, :create]
    post 'attendances/emp_attendance' , to: 'attendances#emp_attendance'
    resources :leaves, :only => [:index, :create, :show, :update]
    resources :overtimes, :only => [:index, :create, :show, :update]
    resources :employee_handbooks, :only => [:index]
    resources :holidays, :only => [:index]
    resources :leave_types, :only => [:index, :create, :show, :update]
    resources :leave_statuses, :only => [:index]
    resources :emergency_phones, :only => [:create, :update]
    resources :employee_roles, :only => [:index]
    resources :additional_detaches, :only => [:index, :destroy]
    resources :additional_allowances, :only => [:index, :destroy]
    resources :leave_type_settings, :only => [:index, :show, :update]
    resources :ot_rate_settings, :only => [:index, :show, :update]
    resources :attendance_settings, :only => [:index, :show, :update]
  end
end
