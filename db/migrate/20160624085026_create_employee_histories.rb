class CreateEmployeeHistories < ActiveRecord::Migration
  def change
    create_table :employee_histories do |t|
      t.integer :employee_id
      t.integer :title_id
      t.integer :department_id
      t.date :start_date
      t.date :end_date

      t.timestamps null: false
    end
  end
end
