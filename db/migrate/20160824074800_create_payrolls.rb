class CreatePayrolls < ActiveRecord::Migration
  def change
    create_table :payrolls do |t|
    	t.integer :employee_id
    	t.date :payroll_month
    	t.integer :basic_pay
    	t.float :overtime_duration
    	t.integer :overtime_payment
    	t.integer :bonus
    	t.integer :commission
    	t.integer :other_plus
    	t.float :total_leave_count
    	t.float :unpaid_leave_count
    	t.integer :leave_deduction
    	t.float :tax
    	t.integer :ssb
    	t.integer :other_detach
    	t.integer :net_pay

      t.timestamps null: false
    end
  end
end
