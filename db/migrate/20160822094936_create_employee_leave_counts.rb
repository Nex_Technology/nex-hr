class CreateEmployeeLeaveCounts < ActiveRecord::Migration
  def change
    create_table :employee_leave_counts do |t|
    	t.integer :employee_id
    	t.integer :leave_type_id
    	t.float :leave_count

      t.timestamps null: false
    end
  end
end
