class CreateFixHolidays < ActiveRecord::Migration
  def change
    create_table :fix_holidays do |t|
      t.string :name
      t.date :date

      t.timestamps null: false
    end
  end
end
