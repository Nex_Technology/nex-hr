class AddBasicPayToEmployeeTbl3 < ActiveRecord::Migration 

 def self.up
  	add_column :employees, :basic_pay, :integer
  end

  def self.down
  	remove_column :employees, :basic_pay, :integer
  end

end
