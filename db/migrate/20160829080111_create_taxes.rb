class CreateTaxes < ActiveRecord::Migration
  def change
    create_table :taxes do |t|
    	t.integer :employee_id
    	t.integer :amount
    	t.string :tax_year

      t.timestamps null: false
    end
  end
end
