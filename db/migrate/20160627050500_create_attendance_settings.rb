class CreateAttendanceSettings < ActiveRecord::Migration
  def change
    create_table :attendance_settings do |t|
      t.time :start_time
      t.time :end_time
      t.integer :define_weekday

      t.timestamps null: false
    end
  end
end
