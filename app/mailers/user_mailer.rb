class UserMailer < ApplicationMailer
	default from: "\"Nex HR\" <no-reply@nex-hr.herokuapp.com/>"

	def password_reset_mail(user)
    @user = user

    @text = "Hello, #{@user.email}!

Someone has requested a link to change your password.

Thanks for joining and have a great day!"

    mail(to: @user.email, subject: 'Reset Password Instruction') do |format|
      format.text { render text: @text }
    end
  end
  
end