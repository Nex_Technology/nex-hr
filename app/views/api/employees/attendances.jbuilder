json.date @month_year.present? ? @month_year : ''
json.total_working_hour @total_working_hour.present? ? @total_working_hour : 0
json.total_late_hour @total_late_hour.present? ? @total_late_hour : 0
json.weekly_attendance @attendance_hash do |kv|
    json.week kv[0]
    json.attendance kv[1] do |e|
    	json.id e.id
    	if e.employee.present? 
				json.employee e.employee, :id, :name
			else
				json.employee empty
			end

			if e.employee.department.present?
				json.department e.employee.department, :id,:name
			else
				json.department empty
			end
			json.date e.date.present? ? e.date.strftime("%Y-%m-%d") : ''
			json.start_time e.start_time.present? ? e.start_time.strftime("%I:%M %p") : ''
			json.end_time e.end_time.present? ? e.end_time.strftime("%I:%M %p") : ''
			json.date e.date.present? ? e.date.strftime("%Y-%m-%d") : ''
			json.is_leave e.is_leave.present? ? e.is_leave : false 
			if e.start_time.present? && e.end_time.present?
				@time = ((e.end_time - e.start_time) / 3600 ).round(2)
				json.total_hour @time.to_s
			else
				json.total_hour ""
			end
    end
end