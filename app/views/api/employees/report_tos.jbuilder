empty = {}
json.array! @report_tos do |r|
	json.id r.id
	json.name r.name
	if r.employee_role.present?
		json.role r.employee_role, :id, :name
	else
		json.role {}
	end
	
	if r.department.present?
		json.department r.department, :id, :name
	else
		json.department empty
	end
	
end