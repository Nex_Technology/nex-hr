json.id @attendance_setting.id
json.start_time @attendance_setting.start_time.present? ? @attendance_setting.start_time.strftime("%H:%M:%S") : ''
json.end_time @attendance_setting.end_time.present? ? @attendance_setting.end_time.strftime("%H:%M:%S")  : ''
json.week_days  @weekdays
