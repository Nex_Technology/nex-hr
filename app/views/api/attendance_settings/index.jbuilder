json.array! @attendance_settings do |a|
	json.id a.id
	json.start_time a.start_time.present? ? a.start_time.strftime("%I:%M %p") : ''
	json.end_time a.end_time.present? ? a.end_time.strftime("%I:%M %p") : ''
	json.week_days  @setting_hash["#{a.id}"]
end