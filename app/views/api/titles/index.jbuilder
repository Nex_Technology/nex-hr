json.array! @titles do |t|
 json.id t.id
 json.name t.name.present? ? t.name : ''
 json.rank t.rank.present? ? t.rank : ''
end