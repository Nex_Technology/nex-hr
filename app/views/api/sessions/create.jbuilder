empty = {}
json.id @employee.id
json.name @employee.name
json.email @employee.email
json.employee_code @employee.employee_code

if @employee.employee_role.present?
	json.employee_role @employee.employee_role, :id, :name
else
	json.employee_role empty
end

if @employee.title.present?
	json.title @employee.title, :id, :name, :rank
else
	json.title empty
end

if @employee.department.present?
	json.department @employee.department, :id, :name, :code
else
	json.department empty
end

json.bank_account @employee.bank_account? ? @employee.bank_account : ''
json.nrc @employee.nrc? ? @employee.nrc : ''
json.dob @employee.dob? ? @employee.dob.strftime("%Y-%m-%d") : ''
json.address @employee.address? ? @employee.address : ''
json.profile_image_photo_original_url @employee.profile_image.present? ? @employee.profile_image.url : ''
json.profile_image_thumb_url @employee.profile_image.present? ? @employee.profile_image.thumb.url : ''
json.profile_image_thumbsamll_url @employee.profile_image.present? ? @employee.profile_image.thumbsmall.url : ''
json.join_date @employee.join_date? ? @employee.join_date.strftime("%Y-%m-%d") : ''

@phones = []
@phones = @employee.phone? ? @employee.phone.split(",") : ''
json.phone @phones

json.personal_email @employee.personal_email? ? @employee.personal_email : ''
json.cv_file @employee.cv_file? ? @employee.cv_file.url : ''
json.performance_appraisal @employee.performance_appraisal.present? ? @employee.performance_appraisal : ''
json.basic_pay @employee.basic_pay.present? ? @employee.basic_pay.to_s : ''
json.gender @employee.gender.present? ? @employee.gender : ''

if @employee.join_date.present?
	today = Date.today
	date1 = today.year*12 + today.month
	date2 = @employee.join_date.year.to_i*12 + @employee.join_date.month.to_i
	month = date1 - date2
	@ser_length = month.divmod(12)
else
	@ser_length = ""
end
json.ser_length @ser_length

json.status @employee.status? ? @employee.status : ''
json.auth_token @employee.auth_token
json.device_token @employee.device_token? ? @employee.device_token : ''
json.finger_print @employee.finger_print? ? @employee.finger_print : ''
json.created_at @employee.created_at? ? @employee.created_at.strftime("%Y-%m-%d %H:%M:%S") : ''
json.updated_at @employee.updated_at? ? @employee.updated_at.strftime("%Y-%m-%d %H:%M:%S") : ''

json.emergency_phones do
  json.array! @employee.emergency_phones do |e_p|
    json.name e_p.name.present? ? e_p.name : ''
    json.phone e_p.phone.present? ? e_p.phone : ''
  end
end