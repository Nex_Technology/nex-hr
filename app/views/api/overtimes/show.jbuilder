empty = {}
json.id @overtime.id
json.requested_date @overtime.requested_date.present? ? @overtime.requested_date.strftime("%Y-%m-%d") : ''

if @overtime.employee.present? 
	json.employee @overtime.employee, :id, :name
else
	json.employee empty
end

if @overtime.employee.department.present?
	json.department @overtime.employee.department, :id, :name
else
	json.department empty
end

json.start_date @overtime.start_date.present? ? @overtime.start_date.strftime("%Y-%m-%d %H:%M:%S") : ''
json.end_date @overtime.end_date.present? ? @overtime.end_date.strftime("%Y-%m-%d %H:%M:%S") : ''
json.ot_hours @overtime.ot_hours.present? ? @overtime.ot_hours : 0.0
json.evaluated_hours @overtime.evaluated_hours.present? ? @overtime.evaluated_hours : 0.0
json.project_name @overtime.project_name.present? ? @overtime.project_name : ''
json.detail_tasks @overtime.detail_tasks.present? ? @overtime.detail_tasks : ''
json.reason @overtime.reason.present? ? @overtime.reason : ''
json.attachment @overtime.attachment.present? ? @overtime.attachment.url : ''
json.attachment_thumb @overtime.attachment.present? ? @overtime.attachment.thumb.url : ''	
json.attachment_thumbsmall @overtime.attachment.present? ? @overtime.attachment.thumbsmall.url : ''

if @overtime.report_to.present?
	json.report_to @overtime.report_to, :id, :name
else
	json.report_to empty
end

if @overtime.approved_by.present?
	json.approved_by @overtime.approved_by, :id, :name
else
	json.approved_by empty
end

json.status @overtime.status.present? ? @overtime.status : ''
json.created_at @overtime.created_at.present? ? @overtime.created_at.strftime("%Y-%m-%d %H:%M:%S") : ''
json.updated_at @overtime.updated_at.present? ? @overtime.updated_at.strftime("%Y-%m-%d %H:%M:%S") : ''