empty = {}
json.array! @leaves do |leave|
	json.id leave.id
	json.requested_date leave.requested_date.present? ? leave.requested_date.strftime("%Y-%m-%d") : ''
	json.start_date leave.start_date.present? ? leave.start_date.strftime("%Y-%m-%d") : ''
	json.end_date leave.end_date.present? ? leave.end_date.strftime("%Y-%m-%d") : ''
	json.is_half_day leave.is_half_day.present? ? leave.is_half_day : false
	json.start_time leave.start_time.present? ? leave.start_time.strftime("%H:%M:%S") : ''
	json.end_time leave.end_time.present? ? leave.end_time.strftime("%H:%M:%S") : ''

	if leave.leave_type.present?
		json.leave_type leave.leave_type, :id, :name, :is_paid
	else
		json.leave_type empty
	end

	if leave.employee.present?
		json.employee leave.employee, :id,:name
	else
		json.employee empty
	end

	if leave.employee.department.present?
		json.department leave.employee, :id,:name
	else
		json.department empty
	end

	if leave.leave_status.present?
		json.leave_status leave.leave_status, :id, :name
	else
		json.leave_status_id empty
	end
end