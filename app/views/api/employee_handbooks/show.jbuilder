json.id @handbook.id
json.name @handbook.name
json.file @handbook.file? ? @handbook.file.url : ''
json.is_published @handbook.is_published.present? ? @handbook.is_published : false