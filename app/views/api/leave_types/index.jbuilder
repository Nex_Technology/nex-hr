json.array! @leave_types do |type|
	json.id type.id.present? ? type.id : ''
	json.name type.name.present? ? type.name : ''
	json.is_paid type.is_paid.present? ? type.is_paid : false
end