empty = {	}
json.array! @payrolls do |p|
	json.id p.id

	if p.employee.department.present?
		json.department p.employee.department, :id, :name
	else
		json.department empty
	end

	if p.employee.present?
		json.employee p.employee, :id, :name
	else
		json.employee empty
	end

	json.payroll_month p.payroll_month
	json.last_generated_date @last_generated_date.present? ? @last_generated_date.updated_at.strftime('%Y-%m-%d') : ''
	json.basic_pay p.basic_pay.present? ? p.basic_pay.to_s : ''
	json.net_pay p.net_pay.present? ? p.net_pay.to_s : ''
end
