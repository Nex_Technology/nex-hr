empty = {}
json.array! @emp_payrolls do |payroll|
	json.id payroll.id

	if payroll.employee.present? 
		json.employee payroll.employee, :id, :name
	else
		json.employee empty
	end
	
	json.payroll_date payroll.payroll_month.present? ? payroll.payroll_month.strftime('%B-%Y') : ''
	json.basic_pay payroll.basic_pay.present? ? payroll.basic_pay.to_s : ""
	json.net_pay payroll.net_pay.present? ? payroll.net_pay.to_s : ""
	json.overtime_payment payroll.overtime_payment.present? ? payroll.overtime_payment.to_s : ""
	json.others_allowance payroll.other_plus.present? ? payroll.other_plus.to_s : "" 
	json.leave_deduction payroll.leave_deduction.present? ? payroll.leave_deduction.to_s : ""
	json.tax payroll.tax.present? ? payroll.tax.to_s : ""
	json.other_detach payroll.other_detach.present? ? payroll.other_detach.to_s : ""
end