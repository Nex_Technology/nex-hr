empty = {	}
json.id @detail.id
json.employee_id @detail.employee_id
json.payroll_date @detail.payroll_month.present? ? @detail.payroll_month.strftime('%B-%Y') : ''
json.basic_pay @detail.basic_pay.present? ? @detail.basic_pay.to_s : ''
json.net_pay @detail.net_pay.present? ? @detail.net_pay.to_s : ''
json.overtime_duration @detail.overtime_duration.present? ? @detail.overtime_duration : ''
json.overtime_payment @detail.overtime_payment.present? ? @detail.overtime_payment.to_s : ''
json.overtimes @overtimes do |overtime|
	json.id overtime.id
	json.requested_date overtime.requested_date.present? ? overtime.requested_date.strftime("%Y-%m-%d") : ''
	json.start_date overtime.start_date.present? ? overtime.start_date.strftime("%Y-%m-%d %H:%M:%S") : ''
	json.end_date overtime.end_date.present? ? overtime.end_date.strftime("%Y-%m-%d %H:%M:%S") : ''
	json.ot_hours overtime.evaluated_hours.present? ? overtime.evaluated_hours : ''
end
json.bonus @detail.bonus.present? ? @detail.bonus.to_s : ''
json.commission @detail.commission.present? ? @detail.commission.to_s : ''

json.other_allowance @other_allowances do |allowance|
	json.id allowance.id
	json.name allowance.name.present? ? allowance.name : ''
	json.amount allowance.amount.present? ? allowance.amount.to_s : ''
end

json.total_leave_count @detail.total_leave_count.present? ? @detail.total_leave_count : ''
json.unpaid_leave_count @detail.unpaid_leave_count.present? ? @detail.unpaid_leave_count : ''

json.leaves @leaves do |leave|
  json.id leave.id
  if leave.leave_type.present? 
  	json.leave_type leave.leave_type, :id, :name
  else
  	json.leave_type empty
  end
  
  json.requested_date leave.requested_date.present? ? leave.requested_date.strftime("%Y-%m-%d") : ''
  json.start_date leave.start_date.present? ? leave.start_date.strftime("%Y-%m-%d") : ''
 	json.end_date leave.end_date.present? ? leave.end_date.strftime("%Y-%m-%d") : ''
 	json.is_half_day leave.is_half_day.present? ? leave.is_half_day : false
 	json.start_time leave.start_time.present? ? leave.start_time.strftime("%H:%M:%S") : ''
 	json.end_time leave.end_time.present? ? leave.end_time.strftime("%H:%M:%S") : ''
end
json.leave_deduction @detail.leave_deduction.present? ? @detail.leave_deduction.to_s : ''
json.tax @detail.tax.present? ? @detail.tax.to_s : ''
json.other_detach @additional_detaches do |detach|
	json.id detach.id
	json.name detach.name.present? ? detach.name : ''
	json.amount detach.amount.present? ? detach.amount.to_s : ''
end