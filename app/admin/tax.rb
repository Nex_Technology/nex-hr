ActiveAdmin.register Tax do

	menu parent: 'Payroll'
	
	permit_params :employee_id, :amount, :tax_year

	index do
	  selectable_column
	  id_column
	  column :employee
	  column :amount
	  column :tax_year
	  actions
	end

	form do |f|
    f.inputs "New Tax" do
		  f.input :employee
		  f.input :amount
		  f.input :tax_year, :as => :select, :collection => options_for_select((Time.zone.now.year - 10)..(Time.zone.now.year + 2))
		 end
    f.actions
	end
end
