ActiveAdmin.register Holiday do

menu parent: 'Holiday'

permit_params :name, :year, :date, :holiday_type, :fix_holiday_id

index do
  selectable_column
  id_column
  column :name
  column :date do |i|
  	i.date.present? ? i.date.strftime("%b, %d") : ''
  end
  column :year
  actions
end

show do |i|
  attributes_table do
    row :id
    row :name
    row :date do
      i.date.present? ? i.date.strftime("%b, %d") : ''
    end
    row :year
  end
end

form do |f|
  f.inputs "New Holiday" do
	  f.input :holiday_type, :label => "Fix Holiday?", :input_html => {
        :onchange => <<-JS
                     if(this.checked == true){
                     	$('#holiday_fix_holiday_input').show();
                     	$('#holiday_date_input').hide();
                     	$('#holiday_name_input').hide();
                     }
                     if(this.checked == false){
	                    $('#holiday_fix_holiday_input').hide();
	                   	$('#holiday_date_input').show();
	                   	$('#holiday_name_input').show();
                     }
                     JS
      }
	  f.input :fix_holiday, :label => "Name"
	  f.input :name
    f.input :date, :discard_year => true
    f.input :year
  end
  f.actions
end

end
