ActiveAdmin.register Overtime do
menu parent: 'Overtime'
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :requested_date, :employee_id, :start_date, :end_date, :ot_hours, :evaluated_hours, :project_name, :detail_tasks, :reason, :attachment, :report_to_id, :approved_by_id, :status

filter :employee
filter :start_date
filter :end_date
filter :ot_hours
filter :project_name
filter :report_to
filter :approved_by
filter :status, as: :select, collection: [['Pending', 1], ['Confirmed', 2],['Declined',3]]

index do
  selectable_column
  id_column
  column :requested_date
  column :employee_id
  column :start_date
  column :end_date
  column :ot_hours
  column :project_name
  actions
end

show do |i|
  attributes_table do
    row :employee
    row :requested_date
    row :start_date
    row :end_date
    row :ot_hours
    row :evaluated_hours
    row :project_name
    row :detail_tasks
    row :reason
    row :attachment
    row :report_to_id
    row :approved_by_id
    row :status do
      case i.status.to_i
      when 1
        "Pending"
      when 2
        "Confirmed"
      when 3
        "Declined"
      end
    end
  end
end

form do |f|
  f.inputs "New Overtime Request" do
  	f.input :employee
    f.input :requested_date, as: :datepicker
    f.input :start_date, as: :datetime_picker
    f.input :end_date, as: :datetime_picker
    f.input :evaluated_hours
    f.input :project_name
    f.input :detail_tasks
    f.input :reason
    f.input :attachment
    f.input :report_to
    f.input :approved_by
    f.input :status,as: :select, :collection =>{"Pending"  => 1,"Confirmed" => 2,"Declined" => 3}
  end
  f.actions
end


end
