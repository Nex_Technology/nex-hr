ActiveAdmin.register Attendance do

menu parent: 'Attendance'

permit_params :employee_id, :start_time, :end_time, :date, :is_leave, :leave_type_id
  
  filter :employee
  filter :is_leave
  filter :date
	
  active_admin_importable do |model, hash|
    employee_code = hash[:employee_code].force_encoding('UTF-8').to_s if hash[:employee_code].present?
    employee = Employee.where(:employee_code => employee_code).first
    hash[:employee] = employee
    hash[:start_time] = hash[:start_time].force_encoding('UTF-8') if hash[:start_time].present?
    hash[:end_time] = hash[:end_time].force_encoding('UTF-8') if hash[:end_time].present?
    hash[:date] = hash[:date].force_encoding('UTF-8') if hash[:date].present?
    hash[:is_leave] = hash[:is_leave].force_encoding('UTF-8') if hash[:is_leave].present?

    if hash[:leave_type].present?
      lt = LeaveType.find_by_name(hash[:leave_type].force_encoding('UTF-8'))
      hash[:leave_type_id] = lt.id
      puts "leave_id #{lt.id}"
      hash.delete(:leave_type)
    end

    hash.delete(:employee_code)
    model.create!(hash)
  end

  csv do
    column (:employee_code) {|obj| obj.employee.present? ? obj.employee.employee_code.to_s : ''}
    column (:employee) {|obj| obj.employee.present? ? obj.employee.name : ''}
    column :start_time
    column :end_time 
    column :date
    column :is_leave
    column(:leave_type) {|obj| obj.leave_type.present? ? obj.leave_type.name : ''}
  end

	index do
    selectable_column
    id_column
    column :employee
    column :start_time
    column :end_time
    column :date
    column :is_leave
    actions
  end

  form do |f|
    f.inputs "New Attendance" do
		  f.input :employee
		  f.input :start_time, as: :time_picker
		  f.input :end_time, as: :time_picker
		  f.input :date, as: :datepicker,:placeholder => "yyyy-mm-dd"
      f.input :is_leave
      f.input :leave_type_id,label: "Leave Type", as: :select, collection: LeaveType.all
    end
    f.actions
	end

end
