ActiveAdmin.register Employee do

menu parent: 'Employee'

permit_params :name, 
	:email, 
	:title_id, 
	:department_id, 
	:profile_image, 
	:dob, 
	:address, 
	:phone, 
	:personal_email, 
	:join_date, 
	:cv_file, 
	:performance_appraisal, 
	:status, 
	:employee_code, 
	:basic_pay, 
	:gender, 
	:password,
	:bank_account,
	:nrc,
	:employee_role_id,
	:emergency_phones_attributes => [
      :id,
      :name,
      :phone,
      :_destroy
    ]
  
  filter :employee_role
	filter :title
	filter :department
	filter :basic_pay
	filter :dob
	filter :join_date
	filter :status, :as => :check_boxes, :collection => proc { Employee.all.pluck(:status).uniq }
	
	index do
    selectable_column
    id_column
    column :name
    column :employee_code
    column :profile_image do |pro|
	    image_tag(pro.profile_image.url(:thumbsmall))
	  end
    column :email
    column :title
    column :department
    column :employee_role
    column :status
    actions
  end

  form do |f|
    f.inputs "New Employee" do
    	f.input :employee_role_id, :as => :select, collection: EmployeeRole.all.map {|r| [r.name, r.id]}
		  f.input :name
		  f.input :email
		  if f.object.new_record?
				f.input :password, :label => "Password"
			else
				f.input :password, :label => "New Password"
			end
		  f.input :title_id, :as => :select, collection: Title.all.map {|t| [t.name + " " + t.rank , t.id]}
		  f.input :department_id, :as => :select, collection: Department.all.map {|d| [d.name, d.id]}
		  f.input :profile_image, :as => :file
		  f.input :bank_account
		  f.input :nrc
		  f.input :dob,:as => :date_picker
		  f.input :address
		  f.input :phone	
		  f.input :gender, as: :select, collection: [['Male', 1], ['Female', 2]]
		  f.input :personal_email
		  f.input :join_date,:as => :date_picker
		  f.input :cv_file,	:as => :file
		  f.input :performance_appraisal
		  f.input :basic_pay
		  f.input :status, as: :select, collection: [['Currently not working', 0], ['Currently working', 1]]
    end
    f.inputs do
      f.has_many :emergency_phones, allow_destroy: true, heading: 'Emergency Phones' do |e_p|
        e_p.input :name
        e_p.input :phone
      end
    end
    f.actions
	end

	#for show page
	show do |a|
	  attributes_table do
	  	row :id
	  	row :employee_role
	    row :name
	    row :employee_code
	    row :email
	    row :title
	    row :department
	    row :profile_image do
	      image_tag(employee.profile_image.url(:thumb))
	    end
	    row :bank_account
	    row :nrc
	    row :dob
	    row :address
	    row :phone
	    row :gender do
        case a.gender.to_i
        when 1
          "Male"
        when 2
          "Female"
        end
      end
	    row :personal_email
	    row :join_date
	    row :cv_file do
	      if a.cv_file.present?
	        ("<a href ='#{a.cv_file}' target='_blank'>#{a.cv_file}</a>").html_safe
	      end
	    end
	    row :performance_appraisal
	    row :basic_pay
	    row :status do
        case a.status.to_i
        when 0
          "Currently not working"
        when 1
          "Currently working"
        end
      end
	    row :auth_token
	    row :device_token
	    row :created_at
	    row :updated_at
	  end
	  panel "Emergency Phones" do
      table_for a.emergency_phones do
        column :name
        column :phone
      end
    end
	end

end
