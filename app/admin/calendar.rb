ActiveAdmin.register_page "Calendar" do

 	content title: proc{ I18n.t("active_admin.calendar") } do
 		div class: "blank_slate_container", id: "calendar_default_message" do
      div class: "side1" do
      end
      div class: "calendar" do
			end
			div class: "side2" do
      end
		end
	end

end