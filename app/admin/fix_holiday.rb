ActiveAdmin.register FixHoliday do

menu parent: 'Holiday'

permit_params :name, :date

index do
  selectable_column
  id_column
  column :name
  column :date do |i|
  	i.date.strftime("%b, %d")
  end
  actions
end

show do |i|
  attributes_table do
    row :id
    row :name
    row :date do
      i.date.strftime("%b, %d")
    end
  end
end

form do |f|
  f.inputs "New Fix Holiday" do
	  f.input :name
    f.input :date, :discard_year => true
  end
  f.actions
end

end
