ActiveAdmin.register EmployeeHistory do

menu parent: 'Employee'
actions :index, :show

permit_params :employee_id, :title_id, :department_id, :start_date, :end_date

filter :employee
filter :title
filter :department
filter :start_date
filter :end_date

index do
  selectable_column
  id_column
  column :employee
  column :title
  column :department
  column :start_date
  column :end_date
  actions
end

end
