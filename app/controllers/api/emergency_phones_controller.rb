class Api::EmergencyPhonesController < Api::BaseController

	def create
		@employee = Employee.where(:id => params[:employee_id]).first
		return render json: { message: "Invalid Employee!"} if @employee.blank?

		@emergency_phone = EmergencyPhone.new(
      employee_id: params[:employee_id],
      name: params[:name],
      phone: params[:phone]
    )
    if @emergency_phone.save!
      respond_to do |format|
      	format.html
				format.json { render josn: @employee, status: 200}
      end
    else 
      return render json: { message: "#{@emergency_phone.errors.first.first} #{@emergency_phone.errors.first.second}" }, status: 400
    end
	end

  def update
    @emergency_phone = EmergencyPhone.where(:id => params[:id]).first
    return render json: { message: "Invalid Emergency Phone! "} if @emergency_phone.blank?

    if @emergency_phone.update(permit_params)
      respond_to do |format|
        format.html
        format.json{ render json: @emergency_phone, status: 200}
      end
    else
      return render json: { message: @emergency_phone.errors}, status: 404
    end
  end

  private
  def permit_params
    params.permit(:employee_id, :name, :phone)
  end

end
