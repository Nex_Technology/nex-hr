class Api::AdditionalDetachesController < Api::BaseController
	
	def index
		if params[:page].present?
			@additional_detaches = AdditionalDetach.all.paginate(per_page: 5, page: params[:page]).order("detach_month DESC, name ASC")
		else
			@additional_detaches = AdditionalDetach.all
		end

		respond_to do |format|
			format.html
			format.json{ render json: @additional_detaches, status: 200}
		end
	end

	def destroy
		@additional_detach = AdditionalDetach.where(:id => params[:id]).first
		return render json: { message: "Invalid Additional Detach!"} if @additional_detach.blank?

		if @additional_detach.destroy
			return render json: { message: "Deleted Successfully!"}, status: 200
		else
			return render json: { message: @additional_detach.errors} ,status: 404
		end
	end

end
