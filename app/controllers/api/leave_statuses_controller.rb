class Api::LeaveStatusesController < Api::BaseController
	def index
		@leave_statuses = LeaveStatus.all

		respond_to do |format|
			format.html
			format.json{ render json: @leave_statuses, status: 200}
		end
	end
end
