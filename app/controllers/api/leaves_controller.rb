class Api::LeavesController < Api::BaseController

	def index
		if params[:page].present?
			@leaves = Leave.all.joins(:employee,:employee => :department).paginate(per_page: 5, page: params[:page]).order("requested_date DESC, departments.name ASC, employees.name ASC")
		else
			@leaves = Leave.all
		end

		respond_to do |format|
			format.html
			format.json{ render json: @leaves, status: 200}
		end
	end

	def create
		@leave = Leave.new(permit_params)
		if @leave.save
			respond_to do |format|
	      format.html
	      format.json{ render json: @leave, status: 200}
			end
		else
			return render json: { message: @leave.errors}, status: 404
		end
	end

	def show
		@leave = Leave.where(id: params[:id]).first
		return render json: { message: 'Invalid Leaves'}, status: 400 if @leave.blank?
		
		respond_to do |format|
      format.html
      format.json{ render json: @leave, status: 200}
		end
	end

	def update
		@leave = Leave.where(id: params[:id]).first
		return render json: { message: 'Invalid Leave'}, status: 400 if @leave.blank?
		if @leave.update(permit_params)
			respond_to do |format|
	      format.html
	      format.json{ render json: @leave, status: 200}
			end
		else
			return render json: { message: @leave.errors}, status: 404
		end
	end

	private
	def permit_params
    params.permit(:requested_date, :start_date, :end_date, :is_half_day, :start_time, :end_time, :leave_type_id, :reason, :attachment, :employee_id, :first_approved_by_id, :second_approved_by_id, :leave_status_id)
  end


end