class Api::EmployeeHandbooksController < Api::BaseController
	
	def index
		@handbook = EmployeeHandbook.where(:is_published => true).first
		respond_to do |format|
	      format.html
	      format.json{ render json: @handbook, status: 200}
		end
	end

	# def show
	# 	@handbook = EmployeeHandbook.where("id =? AND is_published = ?",params[:id],true).first
	# 	return render json: { message: 'Invalid Employee Handbook'}, status: 400 if @handbook.blank?
	# 	respond_to do |format|
	# 		format.html
	# 		format.json { render json: @handbook, status: 200}
	# 	end
	# end

end
