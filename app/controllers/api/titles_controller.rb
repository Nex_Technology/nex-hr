class Api::TitlesController < Api::BaseController
  
	def index
		@titles = Title.all
		respond_to do |format|
	      format.html
	      format.json{ render json: @titles, status: 200}
		end
	end
end
