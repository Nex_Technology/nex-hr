class Api::EmployeesController < Api::BaseController

	def index
		if params[:page].present?
    	@employees = Employee.all.paginate(per_page: 5, page: params[:page])
    else
      @employees = Employee.all
    end

		if @employees.present?
			respond_to do |format|
				format.html
				format.json { render josn: @employees, status: 200}
			end
		end
	end

	def show
		@employee = Employee.where(id: params[:id]).first
		return render json: { message: 'Invalid Employee'}, status: 400 if @employee.blank?

		if @employee.join_date.present?
			today = Date.today
		 
			date1 = today.year*12 + today.month
			date2 = @employee.join_date.year.to_i*12 + @employee.join_date.month.to_i
			month = date1 - date2
			@ser_length = month.divmod(12)
		else 
			@ser_length = []
		end
		
		respond_to do |format|
			format.html
			format.json { render json: @employee, status: 200}
		end
	end

	def update
		@employee = Employee.where(id: params[:id]).first
		return render json: { message: 'Invalid Employee'}, status: 400 if @employee.blank?

		employee_params = JSON.parse(params[:update_values])
		
		if employee_params["phone"].present?
			@phones = employee_params["phone"].join(', ')
		else
			@phones = ""
		end
		@employee.address = employee_params["address"]
		@employee.phone = @phones
		emergency_phones = employee_params["emergency_phone"]
		
		EmergencyPhone.where(:employee_id => @employee.id ).destroy_all
		emergency_phones.each do |e_p|
			EmergencyPhone.create!(
                     :employee_id => @employee.id,
                     :name => e_p["name"],
                     :phone => e_p["phone"]
                     )
		end

		if @employee.save!
			respond_to do |format|
				format.html
				format.json { render json: @employee, status: 200 }
			end
		else
			return render json: { message: @employee.errors}, status: 404
		end
	end

	def change_password
		@employee = Employee.where(id: params[:id]).first
		return render json: { message: 'Invalid Employee'}, status: 400 if @employee.blank?
    return render json: { message: "Require password"}, status: 400 if params[:password].blank?
    return render json: { message: "Require password confirmation!"}, status: 400 if params[:password_confirmation].blank?
    return render json: { message: "Passwords do not match"}, status: 400 unless params[:password]== params[:password_confirmation]

		if @employee.update(password_params)
      sign_in(:employee, @employee)
      return render json: { message: "Password have been updated successfully!"}, status: 200
    else
      return render json: { message: @employee.errors} ,status: 404
    end
	end

	def add_phones
		@employee = Employee.where(id: params[:id]).first
		return render json: { message: 'Invalid Employee'}, status: 400 if @employee.blank?

		if params[:phones].present?
			# get the string inside square brackets
			phones = params[:phones].scan(/(?<=\[)[^\]]+?(?=\])/).last
			# remove white spaces
			@phones = phones.gsub( /\s+/, '' )
		end
		new_phones = @employee.phone + "," + @phones
    @employee.phone = new_phones
    if @employee.save!
    	respond_to do |format|
				format.html
				format.json { render json: @employee, status: 200 }
			end
		end
	end

	def history
		@employee = Employee.where(id: params[:id]).first
		return render json: { message: 'Invalid Employee'}, status: 400 if @employee.blank?

		@history = @employee.employee_histories
		respond_to do |format|
			format.html
			format.json { render json: @history, status: 200 }
		end
	end

	def attendances
		@employee = Employee.where(id: params[:id]).first
		return render json: { message: 'Invalid Employee'}, status: 400 if @employee.blank?
		return render json: { message: 'Require Month!'}, status: 400 if params[:month].blank?
		return render json: { message: 'Require Year!'}, status: 400 if params[:year].blank?
		@month_year = "#{params[:month]}-#{params[:year]}"
		
		@attendances = Attendance.where("employee_id = ? AND extract(month from date) =? AND extract(year from date) =?",params[:id],params[:month],params[:year]).joins(:employee,:employee => :department).order("date DESC, departments.name ASC, employees.name ASC")
		
		@total_working_hour,@total_late_hour = Attendance.working_and_late_hour(@attendances)
		
		@weekly_attendance = []
		@attendances.each do |a|
			# create object inculding attedance object and week attribute
			@weekly_attendance << { :attendance => a, :week => a.date.strftime("%W") }
		end
		
		@week_group = @weekly_attendance.group_by{ |x| x[:week] }.sort_by { |week, attendances| week }
		
		@attendance_hash = Hash.new
		#create hash with "week" and array of attendances
		@week_group.each do |id,values|
		  attendance_arr = []
		  values.each do |v|
		  	attendance_arr << v[:attendance]
		  end
		  @attendance_hash["#{id}"] = attendance_arr
		end
	end

	def web_attendances
		@employee = Employee.where(id: params[:id]).first
		return render json: { message: 'Invalid Employee'}, status: 400 if @employee.blank?
		return render json: { message: 'Require Month!'}, status: 400 if params[:month].blank?
		return render json: { message: 'Require Year!'}, status: 400 if params[:year].blank?
		@month_year = Date.parse("#{params[:month]}-#{params[:year]}").strftime('%B-%Y')
		
		@attendances = Attendance.where("employee_id = ? AND extract(month from date) =? AND extract(year from date) =?",params[:id],params[:month],params[:year]).joins(:employee,:employee => :department).order("date DESC, departments.name ASC, employees.name ASC")
		
		@total_working_hour,@total_late_hour = Attendance.working_and_late_hour(@attendances)
	end

	def overtimes
		@employee = Employee.where(id: params[:id]).first
		return render json: { message: 'Invalid Employee'}, status: 400 if @employee.blank?
		
		@month = params[:month].present? ? params[:month] : Date.today.month
		@year = params[:year].present? ? params[:year] : Date.today.year

		if params[:page].present?
			@overtimes = Overtime.where("employee_id = ? AND (extract(month from start_date) =? OR extract(month from end_date) =? ) AND (extract(year from start_date) =? OR extract(year from end_date) =? )",params[:id],@month,@month,@year,@year).joins(:employee,:employee => :department).paginate(per_page: 5, page: params[:page]).order("requested_date DESC, departments.name ASC, employees.name ASC")
		else
			@overtimes = Overtime.where(:employee_id => params[:id])
		end
	end

	def leaves
		@employee = Employee.where(id: params[:id]).first
		return render json: { message: 'Invalid Employee'}, status: 400 if @employee.blank?
		
		@month = params[:month].present? ? params[:month] : Date.today.strftime("%m")
		@year = params[:year].present? ? params[:year] : Date.today.strftime("%Y")
		
		@leaves = Leave.where("employee_id = ? AND ((extract(month from start_date) =? AND extract(year from start_date) =?) OR (extract(month from end_date) = ? AND extract(year from end_date) = ?))",params[:id],@month,@year,@month,@year)
		@leave_types = LeaveType.all

		@leave_count = Hash.new
		@leave_types.each do |l|
			@leave_count[l.id] = @leaves.where(:leave_type_id => l.id).count
		end

		# service length
		today = Date.today
		date1 = today.year*12 + today.month
		date2 = @employee.join_date.year.to_i*12 + @employee.join_date.month.to_i
		ser_length = (date1 - date2)/12 + 1

		@available_leave_count = Hash.new 
		available_leave = 0
		@leave_types.each do |l|
			available_leave = LeaveTypeSetting.where("experience_year = ? AND leave_type_id = ?",ser_length,l.id).pluck("no_of_leave").first
			@available_leave_count[l.id] = available_leave.present? ? available_leave : 0
		end
	end

	def report_tos
		@report_tos = Employee.where("employee_role_id = ? and status = ?",3,1).order("name ASC")
	end

	def approved_bys
		@approved_bys = Employee.where("employee_role_id = ? and status = ?",3,1).order("name ASC")
	end

	private
	def permit_params
    params.permit(:address)
  end

  def password_params
  	params.permit(:password, :password_confirmation)
  end

end
