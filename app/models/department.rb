class Department < ActiveRecord::Base
	has_many :employees
	has_many :employee_histories
end
