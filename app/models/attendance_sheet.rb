class AttendanceSheet < ActiveRecord::Base
	belongs_to :employee
	belongs_to :submit_by, class_name: "Employee", primary_key: "id"
end
