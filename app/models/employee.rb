class Employee < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :employee_role_id, :name, :email, :title_id, :department_id, :phone, :join_date, :status, :basic_pay, :nrc
  validates :password, presence: true, on: :create

  belongs_to :department
  belongs_to :title
  belongs_to :employee_role
  has_many :employee_histories, :dependent => :destroy
  has_many :attendances, :dependent => :destroy
  has_many :attendance_sheets
  has_many :leaves, :dependent => :destroy
  has_many :employee_leave_counts
  has_many :payrolls, :dependent => :destroy
  has_many :taxes, :dependent => :destroy
  has_many :other_allowances, :dependent => :destroy
  has_many :emergency_phones, :dependent => :destroy

  accepts_nested_attributes_for :emergency_phones, :allow_destroy => true

  mount_uploader :profile_image, ImageUploader
	mount_uploader :cv_file, PdfUploader

  after_save :save_log
  before_create :set_empcode
  def set_empcode
    # get current year
    current_year = Time.now.in_time_zone.to_date.strftime("%y") 
    # gender code => "00"/Male , "50"/Female
    gender_code = if gender == 1 then "00" else "50" end
    # get last emp's code
    # production
    last_emp = Employee.where("employee_code ILIKE ?", "#{current_year.to_s}%").last
    # development
    # last_emp = Employee.where("employee_code LIKE ?", "#{current_year.to_s}%").last
    if last_emp.present?
      last_count = last_emp.employee_code.slice(4..5).to_i
    else
      last_count = 0
    end
    # increase by 1
    last_count = last_count+1
    # set 2 digit
    count = '%02d' % last_count
    # set new emp code
    self.employee_code = "#{current_year}#{gender_code}#{count.to_s}"
  end

  def generate_authentication_token
    self.auth_token = SecureRandom.urlsafe_base64(25).tr('lIO0', 'nex_hr')
  end

  def password_required?
    new_record? ? super : false
  end

  def save_log
    puts "updated_data : #{self.changed_attributes}"

    if self.changed_attributes["title_id"].present? || self.changed_attributes["department_id"].present? 

      last = EmployeeHistory.where(:employee_id => self.id).last
      if last.present?
        last.end_date = self.changed_attributes["updated_at"]
        last.save
        puts "last_record : #{last.id}"
      end

      history = EmployeeHistory.new()
      history.employee_id = self.id
      history.title_id = self.title_id if self.title_id.present?
      history.department_id = self.department_id if self.department_id.present?
      if self.changed_attributes["updated_at"].present?
        history.start_date = self.changed_attributes["updated_at"]
      else
        history.start_date = self.created_at if self.created_at.present?
      end
      history.save
    end

    # add end_date if status changed
    if self.changed_attributes["status"].present?
      last = EmployeeHistory.where(:employee_id => self.id).last
      if last.present?
        last.end_date = self.changed_attributes["updated_at"]
        last.save
        puts "last_record : #{last.id}"
      end
      puts "status: #{self.changed_attributes["status"]}"
    end
    
    # save 4digit employee code
    # production
    employee = Employee.where("employee_code::integer = ?",0).first
    # development
    # employee = Employee.where("employee_code = ?",0).first
    if employee.present?
      unless employee.id.to_s.length > 4
        employee.employee_code = sprintf '%04d', employee.id
        puts employee.employee_code
        employee.save!
      end
    end
    
  end

end
