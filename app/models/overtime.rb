class Overtime < ActiveRecord::Base
	belongs_to :employee
	belongs_to :report_to, class_name: "Employee", primary_key: "id"
	belongs_to :approved_by, class_name: "Employee", primary_key: "id"
	
	mount_uploader :attachment, ImageUploader

	validates_presence_of :employee_id, :report_to, :start_date, :end_date, :requested_date,:project_name
	before_create :save_status

	def save_status
		ot= ((self.end_date - self.start_date) / 3600 ).round(2)
		self.ot_hours = ot
		self.status = 1 if self.status.blank?
	end

	def self.overtime_pay(holiday_arr,employee,month,year,hourly_rate)

		d = Date.parse("#{year}-#{month}-01")
	 	sundays = (d.at_beginning_of_month..d.at_end_of_month).to_a.select {|day| day.sunday? }
	 	# double_days = []
	 	# double_days << (sundays + holiday_arr).uniq
	 	double_days = (sundays + holiday_arr).uniq
	 	# return double_days

  	# production
  	overtimes = Overtime.where("employee_id = ? AND status = ? AND ((extract(month from start_date) = ? AND extract(year from start_date) = ?) OR (extract(month from end_date)=? AND extract(year from end_date)=?))",employee.id,2,month,year,month,year)
  	# development
		# overtimes = Overtime.where("employee_id = ? AND status = ? AND ((strftime('%m',start_date) = ? AND strftime('%Y',start_date) = ?) OR (strftime('%m',end_date) = ? AND strftime('%Y',end_date) = ? ))",employee.id,2,"#{month}",year,"#{month}",year)

		@super_ot_hour = 0
		@normal_ot_hour = 0 
		@holiday_overtime = []

 		@super_overtimes = overtimes.where("date(start_date) IN (?) AND date(end_date) IN (?)", double_days,double_days)
 		@normal_overtimes = overtimes.where("date(start_date) NOT IN (?) AND date(end_date) NOT IN (?)", double_days,double_days)

 		if @super_overtimes.present?
			@super_overtimes.each do |super_ot|
				@super_ot_hour += super_ot.evaluated_hours
			end
			@super_ot_payment = (@super_ot_hour * (hourly_rate.to_i * 2)).round
		end

		if @normal_overtimes.present?
			@normal_overtimes.each do |normal_ot|
				@normal_ot_hour += normal_ot.evaluated_hours
			end
			@normal_ot_payment = (@normal_ot_hour * (hourly_rate.to_i * 1.5)).round
		end

		@ot_hours = @super_ot_hour.to_f + @normal_ot_hour.to_f
		@overtime_payment = @super_ot_payment.to_i + @normal_ot_payment.to_i

		return @ot_hours,@overtime_payment
	end

end
