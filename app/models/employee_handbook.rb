class EmployeeHandbook < ActiveRecord::Base

	validates_presence_of :name, :file

	mount_uploader :file, PdfUploader
	
end
