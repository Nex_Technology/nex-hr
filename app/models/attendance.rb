class Attendance < ActiveRecord::Base
	belongs_to :employee
	belongs_to :leave_type

	# calculate total working hour of a month
	def self.working_and_late_hour(attendances)
		total_working_hour = 0
		total_late_hour = 0
		start_time = Time.zone.parse("2000-01-01 10:00:00")
		attendances.each do |a|
			
			if a.start_time.present? && a.end_time.present?
				# calculate working hours
				w_time = ((a.end_time - a.start_time) / 1.hour).round
				total_working_hour = total_working_hour + w_time

				# calculate late hours
				start_hour = a.start_time.hour
				start_min = a.start_time.min
				if start_hour > start_time.hour || (start_hour = start_time.hour && start_min > start_time.min)
					l_time = ((a.start_time - start_time) / 1.hour).round
					total_late_hour = total_late_hour + l_time
				end
			end
		end
		return total_working_hour,total_late_hour
	end
	
end
