class Tax < ActiveRecord::Base
	belongs_to :employee

	def self.tax_amount(id,year)
		tax = Tax.where("employee_id = ? AND tax_year = ?",id,year).first
		monthly_tax = 0
		if tax.present?
			monthly_tax = (tax.amount.to_i / 12).round if tax.amount.to_i > 0
		end

		return monthly_tax
	end
end
