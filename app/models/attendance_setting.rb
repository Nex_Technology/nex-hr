class AttendanceSetting < ActiveRecord::Base
	
	# def week_days
 #    [
 #        ['sunday', 1],
 #        ['monday', 2],
 #        ['tuesday', 3],
 #        ['wednesday', 4],
 #        ['thursday', 5],
 #        ['friday', 6],
 #        ['saturday', 7]
 #    ]
 #  end
 
	enum days: { monday: 1, tuesday: 2, wednesday: 3, thursday: 4, friday: 5, saturday: 6, sunday: 7 }

  before_validation :show_result
  def show_result
    self.week_days = JSON.parse(self.week_days).collect(&:to_i)
  end

  def self.working_days(date)
  	attendance_setting = self.last
    week_days = attendance_setting.week_days.tr('[]', '').split(',').map(&:to_i)
    working_hour = ((attendance_setting.start_time - attendance_setting.end_time) / 1.hour).round.abs

  	day_of_year = 0
    week_days.each do |a|
      case a
      when 1
        day_of_year += (date.at_beginning_of_year..date.at_end_of_year).count {|day| day.monday? }
      when 2
        day_of_year += (date.at_beginning_of_year..date.at_end_of_year).count {|day| day.tuesday? }
      when 3
        day_of_year += (date.at_beginning_of_year..date.at_end_of_year).count {|day| day.wednesday? }
      when 4
        day_of_year += (date.at_beginning_of_year..date.at_end_of_year).count {|day| day.thursday? }
      when 5
        day_of_year += (date.at_beginning_of_year..date.at_end_of_year).count {|day| day.friday? }
      when 6
        day_of_year += (date.at_beginning_of_year..date.at_end_of_year).count {|day| day.saturday? }
      when 7
        day_of_year += (date.at_beginning_of_year..date.at_end_of_year).count {|day| day.sunday? }
      end
    end
    puts " Week Days =====> #{week_days}"
    puts " Working Days =====> #{day_of_year}"
    puts " Working Hour =====> #{working_hour}"
    return day_of_year, working_hour
  end

end

